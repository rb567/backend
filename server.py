from flask import Flask, request, jsonify
# from flask_cors import cross_origin, CORS
from flask import Flask
from flask_cors import CORS
import firebase_admin
from firebase_admin import credentials
from firebase_admin import firestore
import os
from google.cloud import firestore


os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = "tenant-4db0c-firebase-adminsdk-u3bqq-775e4b7a1b.json"


# Initialize Firebase Admin SDK with your credentials
cred = credentials.Certificate(
    "tenant-4db0c-firebase-adminsdk-u3bqq-775e4b7a1b.json")
firebase_admin.initialize_app(cred)

# Initialize Firestore client
db = firestore.Client()

app = Flask(__name__)
CORS(app)

# Allow requests from your frontend
# CORS(app, resources={r"/tenants/*": {"origins": "http://localhost:3000"}})


# @app.route("/")
# def helloWorld():
#     return "Hello, cross-origin-world!"


@app.route("/tenants", methods=["POST"])
# @cross_origin()
def add_tenant():
    try:
        data = request.json
        print(data)
        tenantid = data["tenantid"]
        tenant_ref = db.collection("Tenant").document(tenantid)
        tenant_ref.set(data)
        return jsonify({"message": "Tenant added successfully", "tenant_id": tenantid}), 201
    except Exception as e:
        return jsonify({"error": str(e)}), 400


@app.route("/tenants/<tenantid>", methods=["PUT"])
# @cross_origin()
def update_tenant(tenantid):
    try:
        data = request.json
        tenant_ref = db.collection("Tenant").document(tenantid)
        tenant_ref.update(data)
        return jsonify({"message": "Tenant updated successfully"}), 200
    except Exception as e:
        return jsonify({"error": str(e)}), 400

# Endpoint for deleting a tenant by ID


@app.route("/tenants/<tenantid>", methods=["DELETE"])
# @cross_origin()
def delete_tenant(tenantid):
    try:
        tenant_ref = db.collection("Tenant").document(tenantid)
        tenant_ref.delete()
        return jsonify({"message": "Tenant deleted successfully"}), 200
    except Exception as e:
        return jsonify({"error": str(e)}), 400


@app.route("/tenants/<search_query>", methods=["GET"])
# @cross_origin()
def search_tenant(search_query):
    try:
        # Initialize Firestore client and reference to the Tenant collection
        db = firestore.Client()
        tenant_ref = db.collection("Tenant")

        # Query Firestore for documents that match either tenantid or tenantName
        query_result = tenant_ref.where(
            "tenantid", "==", search_query).stream()

        # Check if any documents match the tenantID
        for doc in query_result:
            document_data = doc.to_dict()
            # Add this line for logging
            print("Received request for tenantid:", search_query)
            return jsonify(document_data), 200

        # If no documents matched tenantID, try searching by tenantName
        query_result = tenant_ref.where(
            "tenantName", "==", search_query).stream()

        # Check if any documents match the tenantName
        for doc in query_result:
            document_data = doc.to_dict()
            # Add this line for logging
            print("Received request for tenantName:", search_query)
            return jsonify(document_data), 200

        # If no matches were found, return a 404 response
        print("No such document!")
        return jsonify({"message": "Tenant not found"}), 404

    except Exception as e:
        return jsonify({"error": str(e)}), 500


#  find_tenant_by_query
def find_tenant_by_query(query):
    try:
        # Initialize Firestore client and reference to the Tenant collection
        db = firestore.Client()
        tenant_ref = db.collection("Tenant")

        # Query Firestore for documents that match the query
        query_result = tenant_ref.where("tenantid", "==", query).stream()

        # Check if any documents match the tenantID
        for doc in query_result:
            return doc.to_dict()

        # If no documents matched tenantID, try searching by tenantName
        query_result = tenant_ref.where("tenantName", "==", query).stream()

        # Check if any documents match the tenantName
        for doc in query_result:
            return doc.to_dict()

        # If no matches were found, return None
        return None

    except Exception as e:
        print("Error:", str(e))
        return None


@app.route("/tenant", methods=["GET"])
# @cross_origin()
def get_all_tenants():
    try:
        # Reference to the Tenant collection
        tenant_ref = db.collection("Tenant")

        # Query Firestore to get all tenant documents
        query_result = tenant_ref.stream()

        # Use a list comprehension to extract data from documents
        tenant_data = [doc.to_dict() for doc in query_result]

        return jsonify(tenant_data), 200

    except Exception as e:
        return jsonify({"error": str(e)}), 500


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5000)
